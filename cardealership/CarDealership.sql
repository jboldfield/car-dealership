DROP DATABASE IF EXISTS CarDealership;
CREATE DATABASE CarDealership;
USE CarDealership;

CREATE TABLE user(
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(25) NOT NULL,
    last_name VARCHAR(25) NOT NULL,
    email VARCHAR(50) NOT NULL,
    role VARCHAR(15) NOT NULL,
    password VARCHAR(50) NOT NULL
);

CREATE TABLE vehicle(
    vehicle_id INT PRIMARY KEY AUTO_INCREMENT,
    make_id INT NOT NULL,
    model_id INT NOT NULL,
    type VARCHAR(10) NOT NULL,
    body_style varchar(15) NOT NULL,
    year varchar(5) NOT NULL,
    transmission varchar(15) NOT NULL,
    color varchar(15) NOT NULL,
    interior varchar(15) NOT NULL,
    mileage int NOT NULL,
    vin varchar(20) NOT NULL,
    msrp int NOT NULL,
    sales_price int NOT NULL,
    description MEDIUMTEXT NOT NULL,
    featured boolean NOT NULL
);
    

CREATE TABLE sale(
    sale_id INT PRIMARY KEY AUTO_INCREMENT,
    vehicle_id INT NOT NULL,
    name VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    street_1 VARCHAR(20) NOT NULL,
    street_2 VARCHAR(20) NOT NULL,
    city VARCHAR(25) NOT NULL,
    state VARCHAR(5) NOT NULL,
    zipcode VARCHAR(15) NOT NULL,
    purchase_price int NOT NULL,
    purchase_type VARCHAR(25) NOT NULL,
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(vehicle_id)
);

CREATE TABLE make(
    make_id INT PRIMARY KEY AUTO_INCREMENT,
    user_id int NOT NULL,
    make VARCHAR(15) NOT NULL,
    date_added DATE NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(user_id)
);

CREATE TABLE model(
    model_id INT PRIMARY KEY AUTO_INCREMENT,
    user_id int NOT NULL,
    make_id int NOT NULL,
    model VARCHAR(15) NOT NULL,
    date_added DATE NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(user_id),
    FOREIGN KEY (make_id) REFERENCES make(make_id)
);

CREATE TABLE special(
    special_id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(35) NOT NULL,
    description MEDIUMTEXT NOT NULL
);

CREATE TABLE contact(
    contact_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    phone VARCHAR(50) NOT NULL,
    message MEDIUMTEXT NOT NULL
);

INSERT INTO user VALUES
    (1, "Eric", "Wise", "ewise@guildcars.com", "Admin", "password"),
    (2, "Austin", "Hill", "ahill@guildcars.com", "Sales", "password");
    
INSERT INTO make VALUES
    (1, 1, "Audi", STR_TO_DATE("12-1-2021", "%m-%d-%Y")),
    (2, 1, "Fiat", STR_TO_DATE("12-1-2021", "%m-%d-%Y")),
    (3, 1, "Ford", STR_TO_DATE("12-2-2021", "%m-%d-%Y"));
    
INSERT INTO model VALUES
    (1, 1, 1, "A4", STR_TO_DATE("12-2-2021", "%m-%d-%Y")),
    (2, 1, 1, "A6", STR_TO_DATE("12-2-2021", "%m-%d-%Y")),
    (3, 1, 1, "A8", STR_TO_DATE("12-3-2021", "%m-%d-%Y")),
    (4, 1, 2, "Spider", STR_TO_DATE("12-3-2021", "%m-%d-%Y")),
    (5, 1, 3, "F-150", STR_TO_DATE("12-3-2021", "%m-%d-%Y"));
    
INSERT INTO special VALUES
    (1, "Special 1", "this is the first special"),
    (2, "Special 2", "this is the second special"),
    (3, "Special 3", "this is the third special");
    
INSERT INTO contact VALUES
    (1, "Mark Reyes", "mreyes@email.com", "1867655466", "I am only interested in new vehicles");

INSERT INTO vehicle VALUES
    (1, 1, 1, "New", "Sedan", "2021", "Automatic", "Black", "Black", 120, "2DS82019D78S123", 105000, 108000, "This is a new black Audi A4", true),
    (2, 1, 1, "New", "Sedan", "2021", "Automatic", "Black", "Black", 112, "H658765GB78658B", 105000, 108000, "This is a second  new black Audi A4", false),
    (3, 1, 1, "New", "Sedan", "2021", "Automatic", "Red", "Black", 80, "7G765G654FG593G", 105000, 108000, "This is a new red Audi A4", true),
    (4, 1, 2, "New", "Sedan", "2021", "Automatic", "Black", "Black", 160, "8L768746T7G0983", 105000, 108000, "This is a new black Audi A6", true),
    (5, 3, 5, "Used", "truck", "2018", "Automatic", "Blue", "Black", 93, "K86545G879J656L", 34200, 20860, "This is a used blue Ford F-150", false);

INSERT INTO sale VALUES
    (1, 1, "Customer 1", "customer@email.com", "address part 1", "address part 2", "Los Angeles", "CA", "90001", 104670, "Dealer Finance");

SELECT * from user;
SELECT * from make;
SELECT * from model;
SELECT * from special;
SELECT * from contact;
SELECT * from vehicle;
SELECT * from sale;