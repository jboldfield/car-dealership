DROP DATABASE IF EXISTS CarDealershipTest;
CREATE DATABASE CarDealershipTest;
USE CarDealershipTest;

CREATE TABLE user(
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(25) NOT NULL,
    last_name VARCHAR(25) NOT NULL,
    email VARCHAR(50) NOT NULL,
    role VARCHAR(15) NOT NULL,
    password VARCHAR(50) NOT NULL
);

CREATE TABLE vehicle(
    vehicle_id INT PRIMARY KEY AUTO_INCREMENT,
    make_id INT NOT NULL,
    model_id INT NOT NULL,
    type VARCHAR(10) NOT NULL,
    body_style varchar(15) NOT NULL,
    year varchar(5) NOT NULL,
    transmission varchar(15) NOT NULL,
    color varchar(15) NOT NULL,
    interior varchar(15) NOT NULL,
    mileage int NOT NULL,
    vin varchar(20) NOT NULL,
    msrp int NOT NULL,
    sales_price int NOT NULL,
    description MEDIUMTEXT NOT NULL,
    featured boolean NOT NULL
);
    

CREATE TABLE sale(
    sale_id INT PRIMARY KEY AUTO_INCREMENT,
    vehicle_id INT NOT NULL,
    name VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    street_1 VARCHAR(20) NOT NULL,
    street_2 VARCHAR(20) NOT NULL,
    city VARCHAR(25) NOT NULL,
    state VARCHAR(5) NOT NULL,
    zipcode VARCHAR(15) NOT NULL,
    purchase_price int NOT NULL,
    purchase_type VARCHAR(25) NOT NULL,
    FOREIGN KEY (vehicle_id) REFERENCES vehicle(vehicle_id)
);

CREATE TABLE make(
    make_id INT PRIMARY KEY AUTO_INCREMENT,
    user_id int NOT NULL,
    make VARCHAR(15) NOT NULL,
    date_added DATE NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE
);

CREATE TABLE model(
    model_id INT PRIMARY KEY AUTO_INCREMENT,
    user_id int NOT NULL,
    make_id int NOT NULL,
    model VARCHAR(15) NOT NULL,
    date_added DATE NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user(user_id) ON DELETE CASCADE, 
    FOREIGN KEY (make_id) REFERENCES make(make_id)
);

CREATE TABLE special(
    special_id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(35) NOT NULL,
    description MEDIUMTEXT NOT NULL
);

CREATE TABLE contact(
    contact_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    email VARCHAR(50) NOT NULL,
    phone VARCHAR(50) NOT NULL,
    message MEDIUMTEXT NOT NULL
);