package com.sg.cardealership.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sg.cardealership.entity.Contact;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.Model;
import com.sg.cardealership.entity.Sale;
import com.sg.cardealership.entity.Special;
import com.sg.cardealership.entity.User;
import com.sg.cardealership.entity.Vehicle;
import com.sg.cardealership.service.CarDealershipService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/admin")

public class Admin_Controller {
	
	
 	@Autowired
	public CarDealershipService serv;
 	
 	public Contact contact;
	public Make make;
	public Model model;
	public Sale sale;
	public Special special;
	public User user;
	public Vehicle vehicle;
	
	@Autowired
	public Admin_Controller(CarDealershipService serve) {
		
		this.serv=serv;
	}
	
	   @GetMapping("/vehicles")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<Vehicle>> all_vehicles()  {
	      
		    List<Vehicle> cars = serv.getAllVehicles();
	         return ResponseEntity.ok(cars);
	    }
	   
	   
	   @PostMapping("/addvehicles")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity <Vehicle> add_vehicles(@RequestBody Vehicle veh)  {
	      
		    Vehicle car = serv.addVehicle(veh);
	         return ResponseEntity.ok(car);
	    }
	   
	   @PostMapping("/editvehicles")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity <Vehicle> edit_vehicles(@RequestBody Vehicle veh)  {
	      
		    serv.editVehicle(veh);
	       //  return ResponseEntity.ok();
		    return null;
	    }
	   
	   
	   @PostMapping("/users")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> users()  {
	      
		    List<User>all_user=serv.getAllUsers();
	       //  return ResponseEntity.ok();
	         return ResponseEntity.ok(all_user);
	    }
	
	   @PostMapping("/adduser")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> add_users(@RequestBody User user)  {
	      
		    //List<User>all_user=serv.getAllUsers();
		    serv.addUser(user);
	       //  return ResponseEntity.ok();
	       //  return ResponseEntity.ok(all_user);
		    
		    return null;
	    }
	   
	   
	   @PostMapping("/password")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> change_password(@RequestBody User user)  {
	      
		    //List<User>all_user=serv.getAllUsers();
		    //serv.addUser(user);
		   	serv.changePassword(user);
	       //  return ResponseEntity.ok();
	       //  return ResponseEntity.ok(all_user);
		    
		    return null;
	    }
	   
	   
	   @PostMapping("/makes")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> add_make(@RequestBody Make make)  {
	      
		    //List<User>all_user=serv.getAllUsers();
		    //serv.addUser(user);
		   	serv.addMake(make);
	       //  return ResponseEntity.ok();
	       //  return ResponseEntity.ok(all_user);
		    
		    return null;
	    }
	   
	   
	   @PostMapping("/models")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> add_model(@RequestBody Model model)  {
	      
		    //List<User>all_user=serv.getAllUsers();
		    //serv.addUser(user);
		   	serv.addModel(model);
	       //  return ResponseEntity.ok();
	       //  return ResponseEntity.ok(all_user);
		    
		    return null;
	    }
	   
	   @PostMapping("/special")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> add_special(@RequestBody Special special)  {
	      
		    //List<User>all_user=serv.getAllUsers();
		    //serv.addUser(user);
		   	serv.addSpecial(special);
	       //  return ResponseEntity.ok();
	       //  return ResponseEntity.ok(all_user);
		    
		    return null;
	    }
	   
	   @PostMapping("/editspecial")
	    @ResponseStatus(HttpStatus.CREATED)
	    public ResponseEntity<List<User>> edit_special(@RequestBody Special special)  {
	      
		    //List<User>all_user=serv.getAllUsers();
		    //serv.addUser(user);
		   	serv.deleteSpecial(special.getSpecialId());
	       //  return ResponseEntity.ok();
	       //  return ResponseEntity.ok(all_user);
		    
		    return null;
	    }

}
