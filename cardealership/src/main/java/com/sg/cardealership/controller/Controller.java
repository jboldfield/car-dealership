package com.sg.cardealership.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sg.cardealership.entity.Contact;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.Model;
import com.sg.cardealership.entity.Sale;
import com.sg.cardealership.entity.Special;
import com.sg.cardealership.entity.User;
import com.sg.cardealership.entity.Vehicle;
import com.sg.cardealership.service.CarDealershipService;



@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/home")

public class Controller {
	
    @Autowired
    public CarDealershipService serv;

    public Contact contact;
    public Make make;
    public Model model;
    public Sale sale;
    public Special special;
    public User user;
    public Vehicle vehicle;

    @Autowired
    public Controller(CarDealershipService serve) {

        this.serv=serv;
    }




    @GetMapping("/index")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<List<Vehicle>> featurecar()  {

        List<Vehicle> cars = serv.getFeaturedVehicles();
        return ResponseEntity.ok(cars);
     }






     // missing function on the service layer  

    @PostMapping("/new")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<List<Vehicle>> newcar(@RequestBody Vehicle veh)  {


        List<Vehicle> car_model = serv.getVehicleByModel(""+veh.getMakeId(), ""+veh.getModelId(),veh.getYear(), "New") ;
        return ResponseEntity.ok(car_model);
     }




    @PostMapping("/used")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<List<Vehicle>> usedcar(@RequestBody Vehicle veh)  {

        List<Vehicle> car_model = serv.getVehicleByModel(""+veh.getMakeId(), ""+veh.getModelId(),veh.getYear(), "Used") ;
        return ResponseEntity.ok(car_model);
     }



    @PostMapping("/details/{id}")
     public ResponseEntity<Vehicle>  finddetailsbyId(@PathVariable int id) {

            Vehicle car=serv.getVehicleByID(id);
         return ResponseEntity.ok(car);
     }


    @GetMapping("/specials")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<List<Special>> specials()  {

             List<Special> cars = serv.getAllSpecials();
          return ResponseEntity.ok(cars);
     }



    @PostMapping("/contact")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity contact(@RequestBody Contact contact)  {



       Contact update=serv.createContact(contact.getName(), contact.getEmail(), contact.getPhone(), contact.getMessage());

            return new ResponseEntity(update, HttpStatus.CREATED);
     }



    @GetMapping("/sales")
     @ResponseStatus(HttpStatus.CREATED)
     public ResponseEntity<List<Vehicle>> sales_index()  {

             List<Vehicle> cars_sale=serv.getAllVehiclesForSale();
          return ResponseEntity.ok(cars_sale);
     }

    @PostMapping("/purchase")
     public ResponseEntity findpurchaseId(@RequestBody Sale sale) {

           Sale cars_sale=serv.addSale(sale);
          return ResponseEntity.ok(cars_sale);
     }
    
    
}
