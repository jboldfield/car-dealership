package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Contact;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface ContactDao {
    
    Contact getContactById(int contactId);
    
    List<Contact> getAllContacts();
    
    Contact addContact(Contact contact);
    
    void updateContact(Contact contact);
    
    void deleteContactById(int contactId);
}
