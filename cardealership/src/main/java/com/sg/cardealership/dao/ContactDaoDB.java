package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Contact;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class ContactDaoDB implements ContactDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public ContactDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public Contact getContactById(int contactId) {
        try {
            final String SELECT_CONTACT_BY_ID = "SELECT * FROM contact WHERE contact_id = ?";
            return jdbc.queryForObject(SELECT_CONTACT_BY_ID, new ContactMapper(), contactId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Contact> getAllContacts() {
        final String SELECT_ALL_CONTACTS = "SELECT * FROM contact";
        return jdbc.query(SELECT_ALL_CONTACTS, new ContactMapper());
    }

    @Override
    @Transactional
    public Contact addContact(Contact contact) {
        final String INSERT_CONTACT = "INSERT INTO contact(name, email, phone, message) VALUES(?,?,?,?)";

        jdbc.update(INSERT_CONTACT,
                contact.getName(),
                contact.getEmail(),
                contact.getPhone(),
                contact.getMessage());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        contact.setContactId(newId);
        
        return contact;
    }

    @Override
    public void updateContact(Contact contact) {
        final String UPDATE_CONTACT = "UPDATE contact SET name = ?, email = ?, phone = ?, message = ? WHERE contact_id = ?";
        
        jdbc.update(UPDATE_CONTACT,
                contact.getName(),
                contact.getEmail(),
                contact.getPhone(),
                contact.getMessage(),
                contact.getContactId()); 
    }

    @Override
    public void deleteContactById(int contactId) {
        
        final String DELETE_CONTACT = "DELETE FROM contact WHERE contact_id = ?";
        jdbc.update(DELETE_CONTACT, contactId);
    }
    
    public static final class ContactMapper implements RowMapper<Contact> {
        
        @Override
        public Contact mapRow(ResultSet rs, int index) throws SQLException {
            Contact contact = new Contact();
            
            contact.setContactId(rs.getInt("contact_id"));
            contact.setName(rs.getString("name"));
            contact.setEmail(rs.getString("email"));
            contact.setPhone(rs.getString("phone"));
            contact.setMessage(rs.getString("message"));
            
            return contact;
        }
    }
}
