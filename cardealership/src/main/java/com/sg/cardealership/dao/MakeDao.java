package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Make;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface MakeDao {
    
    Make getMakeById(int makeId);
    
    List<Make> getAllMakes();

    Make addMake(Make make);
        
    void updateMake(Make make);
    
    void deleteMakeById(int makeId);
}
