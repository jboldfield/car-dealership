package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Make;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class MakeDaoDB implements MakeDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public MakeDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public Make getMakeById(int makeId) {
        try {
            final String SELECT_MAKE_BY_ID = "SELECT * FROM make WHERE make_id = ?";
            return jdbc.queryForObject(SELECT_MAKE_BY_ID, new MakeMapper(), makeId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Make> getAllMakes() {
        final String SELECT_ALL_MAKES = "SELECT * FROM make";
        return jdbc.query(SELECT_ALL_MAKES, new MakeMapper());
    }

    @Override
    @Transactional
    public Make addMake(Make make) {
        final String INSERT_MAKE = "INSERT INTO make(make_id, user_id, make, date_added) VALUES(?,?,?,?)";

        jdbc.update(INSERT_MAKE,
                make.getMakeId(), 
                make.getUserId(),
                make.getMake(),
                make.getDateAdded());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        make.setMakeId(newId);
        
        return make;
    }

    @Override
    public void updateMake(Make make) {
        final String UPDATE_MAKE = "UPDATE make SET user_id = ?, make = ?, date_added = ? WHERE make_id = ?";
        
        jdbc.update(UPDATE_MAKE,
                make.getUserId(),
                make.getMake(),
                make.getDateAdded(),
                make.getMakeId()); 
    }

    @Override
    public void deleteMakeById(int makeId) {
        
        final String DELETE_MAKE = "DELETE FROM make WHERE make_id = ?";
        jdbc.update(DELETE_MAKE, makeId);
    }
    
    public static final class MakeMapper implements RowMapper<Make> {
        
        @Override
        public Make mapRow(ResultSet rs, int index) throws SQLException {
            Make make = new Make();
            
            make.setMakeId(rs.getInt("make_id"));
            make.setUserId(rs.getInt("user_id"));
            make.setMake(rs.getString("make"));
            java.sql.Timestamp dateAdded = rs.getTimestamp("date_added");
            make.setDateAdded(dateAdded.toLocalDateTime());
            
            return make;
        }
    }
}
