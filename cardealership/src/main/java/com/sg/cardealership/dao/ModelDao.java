package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Model;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface ModelDao {
    
    Model getModelById(int modelId);
    
    List<Model> getAllModels();

    Model addModel(Model model);
        
    void updateModel(Model model);
    
    void deleteModelById(int modelId);
}
