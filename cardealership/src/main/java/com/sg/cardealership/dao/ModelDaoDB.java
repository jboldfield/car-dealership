package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Model;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class ModelDaoDB implements ModelDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public ModelDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public Model getModelById(int modelId) {
        try {
            final String SELECT_MODEL_BY_ID = "SELECT * FROM model WHERE model_id = ?";
            return jdbc.queryForObject(SELECT_MODEL_BY_ID, new ModelMapper(), modelId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Model> getAllModels() {
        final String SELECT_ALL_MODELS = "SELECT * FROM model";
        return jdbc.query(SELECT_ALL_MODELS, new ModelMapper());
    }

    @Override
    @Transactional
    public Model addModel(Model model) {
        final String INSERT_MODEL = "INSERT INTO model(model_id, user_id, make_id, model, date_added) VALUES(?,?,?,?,?)";

        jdbc.update(INSERT_MODEL,
                model.getModelId(),
                model.getUserId(),
                model.getMakeId(),
                model.getModel(),
                model.getDateAdded());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        model.setModelId(newId);
        
        return model;
    }

    @Override
    public void updateModel(Model model) {
        final String UPDATE_MODEL = "UPDATE model SET user_id = ?, make_id = ?, model = ?, date_added = ? WHERE model_id = ?";
        
        jdbc.update(UPDATE_MODEL,
                model.getUserId(),
                model.getModel(),
                model.getDateAdded(),
                model.getModelId()); 
    }

    @Override
    public void deleteModelById(int modelId) {
        
        final String DELETE_MODEL = "DELETE FROM model WHERE model_id = ?";
        jdbc.update(DELETE_MODEL, modelId);
    }
    
    public static final class ModelMapper implements RowMapper<Model> {
        
        @Override
        public Model mapRow(ResultSet rs, int index) throws SQLException {
            Model model = new Model();
            
            model.setModelId(rs.getInt("model_id"));
            model.setUserId(rs.getInt("user_id"));
            model.setModel(rs.getString("model"));
            java.sql.Timestamp dateAdded = rs.getTimestamp("date_added");
            model.setDateAdded(dateAdded.toLocalDateTime());
            
            return model;
        }
    }
}
