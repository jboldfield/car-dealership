package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Sale;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface SaleDao {
    
    Sale getSaleById(int saleId);
    
    List<Sale> getAllSales();

    Sale addSale(Sale sale);
        
    void updateSale(Sale sale);
    
    void deleteSaleById(int saleId);
}
