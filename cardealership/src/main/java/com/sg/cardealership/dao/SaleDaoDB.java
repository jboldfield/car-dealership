package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Sale;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class SaleDaoDB implements SaleDao {
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public SaleDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public Sale getSaleById(int saleId) {
        try {
            final String SELECT_SALE_BY_ID = "SELECT * FROM sale WHERE sale_id = ?";
            return jdbc.queryForObject(SELECT_SALE_BY_ID, new SaleMapper(), saleId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Sale> getAllSales() {
        final String SELECT_ALL_SALES = "SELECT * FROM sale";
        return jdbc.query(SELECT_ALL_SALES, new SaleMapper());
    }

    @Override
    @Transactional
    public Sale addSale(Sale sale) {
        final String INSERT_SALE = "INSERT INTO sale(sale_id, vehicle_id, name, email, street_1, street_2, city, state, zipcode, purchase_price, purchase_type) VALUES(?,?,?,?,?,?,?,?,?,?,?)";

        jdbc.update(INSERT_SALE,
                sale.getSaleId(), 
                sale.getVehicleId(),
                sale.getName(),
                sale.getEmail(),
                sale.getStreet1(),
                sale.getStreet2(),
                sale.getCity(),
                sale.getState(),
                sale.getZipcode(),
                sale.getPurchasePrice(),
                sale.getPurchaseType());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        sale.setSaleId(newId);
        
        return sale;
    }

    @Override
    public void updateSale(Sale sale) {
        final String UPDATE_SALE = "UPDATE sale SET vehicle_id = ?, name = ?, email = ?, street_1 = ?, street_2 = ?, city = ?, state = ?, zipcode = ?, purchase_price = ?, purchase_type = ? WHERE sale_id = ?";
        
        jdbc.update(UPDATE_SALE,
                sale.getVehicleId(),
                sale.getName(),
                sale.getEmail(),
                sale.getStreet1(),
                sale.getStreet2(),
                sale.getCity(),
                sale.getState(),
                sale.getZipcode(),
                sale.getPurchasePrice(),
                sale.getPurchaseType(),
                sale.getSaleId());
    }

    @Override
    public void deleteSaleById(int saleId) {
        
        final String DELETE_SALE = "DELETE FROM sale WHERE sale_id = ?";
        jdbc.update(DELETE_SALE, saleId);
    }
    
    public static final class SaleMapper implements RowMapper<Sale> {
        
        @Override
        public Sale mapRow(ResultSet rs, int index) throws SQLException {
            Sale sale = new Sale();
            
            sale.setSaleId(rs.getInt("sale_id"));
            sale.setVehicleId(rs.getInt("vehicle_id"));
            sale.setName(rs.getString("name"));
            sale.setEmail(rs.getString("email"));
            sale.setStreet1(rs.getString("street_1"));
            sale.setStreet2(rs.getString("street_2"));
            sale.setCity(rs.getString("city"));
            sale.setState(rs.getString("state"));
            sale.setZipcode(rs.getString("zipcode"));
            sale.setPurchasePrice(rs.getInt("purchase_price"));
            sale.setPurchaseType(rs.getString("purchase_type"));

            
            return sale;
        }
    }
}
