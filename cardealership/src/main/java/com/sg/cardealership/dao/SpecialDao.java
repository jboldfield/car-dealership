package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Special;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface SpecialDao {
    
    Special getSpecialById(int specialId);
    
    List<Special> getAllSpecials();
    
    Special addSpecial(Special special);
    
    void updateSpecial(Special special);
    
    void deleteSpecialById(int specialId);
}
