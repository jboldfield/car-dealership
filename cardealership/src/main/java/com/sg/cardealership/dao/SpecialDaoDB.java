package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Special;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class SpecialDaoDB implements SpecialDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public SpecialDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public Special getSpecialById(int specialId) {
        try {
            final String SELECT_SPECIAL_BY_ID = "SELECT * FROM special WHERE special_id = ?";
            return jdbc.queryForObject(SELECT_SPECIAL_BY_ID, new SpecialMapper(), specialId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Special> getAllSpecials() {
        final String SELECT_ALL_SPECIALS = "SELECT * FROM special";
        return jdbc.query(SELECT_ALL_SPECIALS, new SpecialMapper());
    }

    @Override
    @Transactional
    public Special addSpecial(Special special) {
        final String INSERT_SPECIAL = "INSERT INTO special(title, description) VALUES(?,?)";

        jdbc.update(INSERT_SPECIAL,
                special.getTitle(), 
                special.getDescription());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        special.setSpecialId(newId);
        
        return special;
    }

    @Override
    public void updateSpecial(Special special) {
        final String UPDATE_SPECIAL = "UPDATE Special SET title = ?, description = ? WHERE special_id = ?";
        
        jdbc.update(UPDATE_SPECIAL,
                special.getTitle(),
                special.getDescription(),
                special.getSpecialId()); 
    }

    @Override
    public void deleteSpecialById(int specialId) {
        
        final String DELETE_SPECIAL = "DELETE FROM special WHERE special_id = ?";
        jdbc.update(DELETE_SPECIAL, specialId);
    }
    
    public static final class SpecialMapper implements RowMapper<Special> {
        
        @Override
        public Special mapRow(ResultSet rs, int index) throws SQLException {
            Special special = new Special();
            
            special.setSpecialId(rs.getInt("special_id"));
            special.setTitle(rs.getString("title"));
            special.setDescription(rs.getString("description"));
            
            return special;
        }
    }
}
