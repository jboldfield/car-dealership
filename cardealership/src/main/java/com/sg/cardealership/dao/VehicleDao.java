package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Vehicle;
import java.util.List;

/**
 *
 * @author joldf
 */
public interface VehicleDao {
    
    Vehicle getVehicleById(int vehicleId);
    
    List<Vehicle> getAllVehicles();

    Vehicle addVehicle(Vehicle vehicle);
        
    void updateVehicle(Vehicle vehicle);
    
    void deleteVehicleById(int vehicleId);
    
    List<Vehicle> getVehicleByModel(String make, String model, String year,String type);
}
