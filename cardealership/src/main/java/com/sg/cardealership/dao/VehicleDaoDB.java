package com.sg.cardealership.dao;

import com.sg.cardealership.entity.Vehicle;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author joldf
 */
@Repository
public class VehicleDaoDB implements VehicleDao{
    
    @Autowired
    JdbcTemplate jdbc;
    
    @Autowired
    public VehicleDaoDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }
    
    @Override
    public Vehicle getVehicleById(int vehicleId) {
        try {
            final String SELECT_VEHICLE_BY_ID = "SELECT * FROM vehicle WHERE vehicle_id = ?";
            return jdbc.queryForObject(SELECT_VEHICLE_BY_ID, new VehicleMapper(), vehicleId);
        } catch(DataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Vehicle> getAllVehicles() {
        final String SELECT_ALL_VEHICLES = "SELECT * FROM vehicle";
        return jdbc.query(SELECT_ALL_VEHICLES, new VehicleMapper());
    }

    @Override
    @Transactional
    public Vehicle addVehicle(Vehicle vehicle) {
        final String INSERT_VEHICLE = "INSERT INTO vehicle(vehicle_id, make_id, model_id, type, body_style, year, transmission, color, interior, mileage, vin, msrp, sales_price, description, featured) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        jdbc.update(INSERT_VEHICLE,
                vehicle.getVehicleId(),
                vehicle.getMakeId(),
                vehicle.getModelId(),
                vehicle.getType(),
                vehicle.getBodyStyle(),
                vehicle.getYear(),
                vehicle.getTransmission(),
                vehicle.getColor(),
                vehicle.getInterior(),
                vehicle.getMileage(),
                vehicle.getVin(),
                vehicle.getMsrp(),
                vehicle.getSalesPrice(),
                vehicle.getDescription(),
                vehicle.getFeatured());
        
        int newId = jdbc.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        vehicle.setVehicleId(newId);
        
        return vehicle;
    }

    @Override
    public void updateVehicle(Vehicle vehicle) {
        final String UPDATE_VEHICLE = "UPDATE vehicle SET make_id = ?, model_id = ?, type = ?, body_style = ?, year = ?, transmission = ?, color = ?, interior = ?, mileage = ?, vin = ?, msrp = ?, sales_price = ?, description = ?, featured = ? WHERE vehicle_id = ?";
        
        jdbc.update(UPDATE_VEHICLE,
                vehicle.getMakeId(),
                vehicle.getModelId(),
                vehicle.getType(),
                vehicle.getBodyStyle(),
                vehicle.getYear(),
                vehicle.getTransmission(),
                vehicle.getColor(),
                vehicle.getInterior(),
                vehicle.getMileage(),
                vehicle.getVin(),
                vehicle.getMsrp(),
                vehicle.getSalesPrice(),
                vehicle.getDescription(),
                vehicle.getFeatured(),
                vehicle.getVehicleId());
    }

    @Override
    public void deleteVehicleById(int vehicleId) {
        
        final String DELETE_VEHICLE = "DELETE FROM vehicle WHERE vehicle_id = ?";
        jdbc.update(DELETE_VEHICLE, vehicleId);
    }
    
    public static final class VehicleMapper implements RowMapper<Vehicle> {
        
        @Override
        public Vehicle mapRow(ResultSet rs, int index) throws SQLException {
            Vehicle vehicle = new Vehicle();
            
            vehicle.setVehicleId(rs.getInt("vehicle_id"));
            vehicle.setMakeId(rs.getInt("make_id"));
            vehicle.setModelId(rs.getInt("model_id"));
            vehicle.setType(rs.getString("type"));
            vehicle.setBodyStyle(rs.getString("body_style"));
            vehicle.setYear(rs.getString("year"));
            vehicle.setTransmission(rs.getString("transmission"));
            vehicle.setColor(rs.getString("color"));
            vehicle.setInterior(rs.getString("interior"));
            vehicle.setMileage(rs.getInt("mileage"));
            vehicle.setVin(rs.getString("vin"));
            vehicle.setMsrp(rs.getInt("msrp"));
            vehicle.setSalesPrice(rs.getInt("sales_price"));
            vehicle.setDescription(rs.getString("description"));
            vehicle.setFeatured(rs.getBoolean("featured"));
            
            return vehicle;
        }
    }

    @Override
    public List<Vehicle> getVehicleByModel(String make, String model, String year,String type) {
            // TODO Auto-generated method stub


             final String SELECT_ALL_VEHICLES = "SELECT * FROM vehicle where make_id= ? AND model_id= ? AND year= ? AND type = ?";
            return jdbc.query(SELECT_ALL_VEHICLES, new VehicleMapper(),make,model,year,type);
    }
    
    public List<Vehicle> getAllVehiclesForSale() {
        final String SELECT_ALL_VEHICLES = "SELECT * FROM vehicle WHERE vehicle_id NOT IN (SELECT vehicle_id FROM sale)";
        return jdbc.query(SELECT_ALL_VEHICLES, new VehicleMapper());
    }
	
}
