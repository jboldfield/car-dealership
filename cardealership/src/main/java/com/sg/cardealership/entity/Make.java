package com.sg.cardealership.entity;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author joldf
 */
public class Make {
    
    private int makeId;
    private int userId;
    private String make; // A4
    private LocalDateTime dateAdded;  // Date that user added the make

    public Make() {
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.makeId;
        hash = 89 * hash + this.userId;
        hash = 89 * hash + Objects.hashCode(this.make);
        hash = 89 * hash + Objects.hashCode(this.dateAdded);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Make other = (Make) obj;
        if (this.makeId != other.makeId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if (!Objects.equals(this.make, other.make)) {
            return false;
        }
        if (!Objects.equals(this.dateAdded, other.dateAdded)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Make{" + "makeId=" + makeId + ", userId=" + userId + ", make=" + make + ", dateAdded=" + dateAdded + '}';
    }
    
}
