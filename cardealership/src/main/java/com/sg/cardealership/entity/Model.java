package com.sg.cardealership.entity;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author joldf
 */
public class Model {
    
    private int modelId;
    private int userId;
    private int makeId;
    private String model; // Audi
    private LocalDateTime dateAdded;  // Date user added model

    public Model() {
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public LocalDateTime getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDateTime dateAdded) {
        this.dateAdded = dateAdded;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + this.modelId;
        hash = 11 * hash + this.userId;
        hash = 11 * hash + this.makeId;
        hash = 11 * hash + Objects.hashCode(this.model);
        hash = 11 * hash + Objects.hashCode(this.dateAdded);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Model other = (Model) obj;
        if (this.modelId != other.modelId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if (this.makeId != other.makeId) {
            return false;
        }
        if (!Objects.equals(this.model, other.model)) {
            return false;
        }
        if (!Objects.equals(this.dateAdded, other.dateAdded)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model{" + "modelId=" + modelId + ", userId=" + userId + ", makeId=" + makeId + ", model=" + model + ", date=" + dateAdded + '}';
    }
    
}
