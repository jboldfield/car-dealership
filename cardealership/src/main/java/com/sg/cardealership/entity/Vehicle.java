package com.sg.cardealership.entity;

import java.util.Objects;

/**
 *
 * @author joldf
 */
public class Vehicle {
    
    private int vehicleId;
    private int makeId;
    private int modelId;
    private String type;
    private String bodyStyle; // Car, SUV
    private String year;
    private String transmission; // Automatic, Manual
    private String color;
    private String interior; // Color of interior
    private int mileage;
    private String vin;
    private int msrp; // Manufacturer's sugested retail price
    private int salesPrice;  
    private String description;
    private boolean featured;

    public Vehicle() {
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getMakeId() {
        return makeId;
    }

    public void setMakeId(int makeId) {
        this.makeId = makeId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBodyStyle() {
        return bodyStyle;
    }

    public void setBodyStyle(String bodyStyle) {
        this.bodyStyle = bodyStyle;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getMsrp() {
        return msrp;
    }

    public void setMsrp(int msrp) {
        this.msrp = msrp;
    }

    public int getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(int salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + this.vehicleId;
        hash = 13 * hash + this.makeId;
        hash = 13 * hash + this.modelId;
        hash = 13 * hash + Objects.hashCode(this.type);
        hash = 13 * hash + Objects.hashCode(this.bodyStyle);
        hash = 13 * hash + Objects.hashCode(this.year);
        hash = 13 * hash + Objects.hashCode(this.transmission);
        hash = 13 * hash + Objects.hashCode(this.color);
        hash = 13 * hash + Objects.hashCode(this.interior);
        hash = 13 * hash + this.mileage;
        hash = 13 * hash + Objects.hashCode(this.vin);
        hash = 13 * hash + this.msrp;
        hash = 13 * hash + this.salesPrice;
        hash = 13 * hash + Objects.hashCode(this.description);
        hash = 13 * hash + (this.featured ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehicle other = (Vehicle) obj;
        if (this.vehicleId != other.vehicleId) {
            return false;
        }
        if (this.makeId != other.makeId) {
            return false;
        }
        if (this.modelId != other.modelId) {
            return false;
        }
        if (this.mileage != other.mileage) {
            return false;
        }
        if (this.msrp != other.msrp) {
            return false;
        }
        if (this.salesPrice != other.salesPrice) {
            return false;
        }
        if (this.featured != other.featured) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.bodyStyle, other.bodyStyle)) {
            return false;
        }
        if (!Objects.equals(this.year, other.year)) {
            return false;
        }
        if (!Objects.equals(this.transmission, other.transmission)) {
            return false;
        }
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        if (!Objects.equals(this.interior, other.interior)) {
            return false;
        }
        if (!Objects.equals(this.vin, other.vin)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Vehicle{" + "vehicleId=" + vehicleId + ", makeId=" + makeId + ", modelId=" + modelId + ", type=" + type + ", bodyStyle=" + bodyStyle + ", year=" + year + ", transmission=" + transmission + ", color=" + color + ", interior=" + interior + ", mileage=" + mileage + ", vin=" + vin + ", msrp=" + msrp + ", salesPrice=" + salesPrice + ", description=" + description + ", featured=" + featured + '}';
    }
    
}
