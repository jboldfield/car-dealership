/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.cardealership.service;

import com.sg.cardealership.entity.Contact;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.Model;
import com.sg.cardealership.entity.Sale;
import com.sg.cardealership.entity.Special;
import com.sg.cardealership.entity.User;
import com.sg.cardealership.entity.Vehicle;
import java.time.LocalDateTime;
import java.util.List;

/**
 *
 * @author Riddle
 */
public interface CarDealershipService {
    Model addModel(Model newModel);
    
    Make addMake(Make newMake);
    List<Vehicle> getFeaturedVehicles();
    Sale addSale(Sale newsale);
    
    List<Sale> getAllSales();
    
    List<Vehicle> getAllVehicles();
    
    Vehicle addVehicle(Vehicle newVehicle);
    
    void editVehicle(Vehicle vehicle);
    
    Vehicle getVehicleByID(int id);
    
    //List<Vehicle> getNewVehicles();
    
    //List<Vehicle> getUsedVehicles();
    
    List<Vehicle> getVehicleByModel(String make ,String model,String year,String type);
    
    List<User> getAllUsers();
    
    User addUser(User user);
    
    void editUser(int id, String firstName, String lastName, String email, String role, String password);
    
    void changePassword(User user);
    
    Contact createContact(String name, String email, String phone, String msg);
    
    Sale createSale(int vehicleID, String name, String email, String street1, String street2, String city, String state, String zipcode, int price, String type);
    
    List<Special> getAllSpecials();
    
    Special addSpecial(Special special);
    
    void deleteSpecial(int id);

    public List<Vehicle> getAllVehiclesForSale();
}
