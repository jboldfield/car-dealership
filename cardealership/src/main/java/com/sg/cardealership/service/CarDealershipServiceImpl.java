/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.cardealership.service;

import com.sg.cardealership.dao.ContactDaoDB;
import com.sg.cardealership.dao.MakeDaoDB;
import com.sg.cardealership.dao.ModelDaoDB;
import com.sg.cardealership.dao.SaleDaoDB;
import com.sg.cardealership.dao.SpecialDaoDB;
import com.sg.cardealership.dao.UserDaoDB;
import com.sg.cardealership.dao.VehicleDaoDB;
import com.sg.cardealership.entity.Contact;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.Model;
import com.sg.cardealership.entity.Sale;
import com.sg.cardealership.entity.Special;
import com.sg.cardealership.entity.User;
import com.sg.cardealership.entity.Vehicle;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Riddle
 */

@Component
public class CarDealershipServiceImpl implements CarDealershipService{
    @Autowired
    ContactDaoDB contactDao;
    
    @Autowired
    SpecialDaoDB specialDao;
    
    @Autowired
    UserDaoDB userDao;
    
    @Autowired
    VehicleDaoDB vehicleDao;
    
    @Autowired
    SaleDaoDB saleDao;
    
    @Autowired
    MakeDaoDB makeDao;
    
    @Autowired
    ModelDaoDB modelDao;
    /*private int modelId;
    private int userId;
    private int make_Id;
    private String model; // Audi
    private LocalDateTime dateAdded;*/
    public Model addModel(Model newModel)
    {
		/*
		 * Model newModel = new Model(); newModel.setUserId(userID);
		 * newModel.setMakeId(makeID); newModel.setModel(model);
		 */
        newModel.setDateAdded(LocalDateTime.now());
        
        newModel = modelDao.addModel(newModel);
        return newModel;
    }
    
    public Make addMake(Make newMake)
    {
		/*
		 * Make newMake = new Make(); newMake.setUserId(userID); newMake.setMake(make);
		 */
        newMake.setDateAdded(LocalDateTime.now());
        
        newMake = makeDao.addMake(newMake);
        return newMake;
    }
    
    public Sale addSale(Sale newsale)
    {
        Sale sale = new Sale();
		/*
		 * sale.setVehicleId(vehicleID); sale.setName(name); sale.setEmail(email);
		 * sale.setStreet1(street1); sale.setStreet2(street2); sale.setCity(city);
		 * sale.setState(state); sale.setZipcode(zipcode);
		 * sale.setPurchasePrice(purchasePrice); sale.setPurchaseType(purchaseType);
		 */
        
        sale = saleDao.addSale(newsale);
        return sale;
    }
    
    public List<Sale> getAllSales()
    {
        return saleDao.getAllSales();
    }
    
    public List<Vehicle> getAllVehicles()
    {
        return vehicleDao.getAllVehicles();
    }
    
    public Vehicle addVehicle(Vehicle newVehicle)
    {
        //Vehicle newVehicle = new Vehicle();
		/*
		 * newVehicle.setMakeId(makeID); newVehicle.setModelId(modelID);
		 * newVehicle.setType(type); newVehicle.setBodyStyle(bodyStyle);
		 * newVehicle.setYear(year); newVehicle.setTransmission(transmission);
		 * newVehicle.setColor(color); newVehicle.setInterior(interior);
		 * newVehicle.setMileage(mileage); newVehicle.setVin(vin);
		 * newVehicle.setMsrp(msrp); newVehicle.setSalesPrice(salesPrice);
		 * newVehicle.setDescription(description);
		 */
        
        newVehicle = vehicleDao.addVehicle(newVehicle);
        return newVehicle;
    }
    
    public void editVehicle(Vehicle vehicle) {
     /*   Vehicle vehicle = vehicleDao.getVehicleById(id);
        vehicle.setMakeId(makeID);
        vehicle.setModelId(modelID);
        vehicle.setType(type);
        vehicle.setBodyStyle(bodyStyle);
        vehicle.setYear(year);
        vehicle.setTransmission(transmission);
        vehicle.setColor(color);
        vehicle.setInterior(interior);
        vehicle.setMileage(mileage);
        vehicle.setVin(vin);
        vehicle.setMsrp(msrp);
        vehicle.setSalesPrice(salesPrice);
        vehicle.setDescription(description);*/
        
        vehicleDao.updateVehicle(vehicle);
    }
    
    public Vehicle getVehicleByID(int id)
    {
        return vehicleDao.getVehicleById(id);
    }
    

    public List<Vehicle> getVehicleByModel(String make ,String model,String year,String type)
    {
        return vehicleDao.getVehicleByModel(make,model,year,type);
    }
    
    
    /*public List<Vehicle> getNewVehicles()
    {
        List<Vehicle> vehicles = vehicleDao.getAllVehicles();
        people.stream().filter((p) -> p.getAge() >= 18).collect(Collectors.toList());
    }
    
    public List<Vehicle> getUsedVehicles()
    {
        List<Vehicle> vehicles = vehicleDao.getAllVehicles();
        people.stream().filter((p) -> p.getAge() >= 18).collect(Collectors.toList());
    }*/
    
    public List<User> getAllUsers()
    {
        return userDao.getAllUsers();
    }
    
    public User addUser(User user)
    {
		/*
		 * User user = new User(); user.setUserId(id); user.setFirstName(firstName);
		 * user.setLastName(lastName); user.setEmail(email); user.setRole(role);
		 * user.setPassword(password);
		 */
        
        user = userDao.addUser(user);
        return user;
    }
    
    public void editUser(int id, String firstName, String lastName, String email, String role, String password)
    {
        User user = userDao.getUserById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setRole(role);
        if(!password.equals("")) user.setPassword(password);
        
        userDao.updateUser(user);
        return;
    }
    
    public void changePassword(User user)
    {
		/*
		 * User user = userDao.getUserById(id); user.setPassword(password);
		 */
        
        userDao.updatePassword(user);
        return;
    }
    
    public Contact createContact(String name, String email, String phone, String msg)
    {
        Contact newContact = new Contact();
        newContact.setName(name);
        newContact.setEmail(email);
        newContact.setPhone(phone);
        newContact.setMessage(msg);
        
        newContact = contactDao.addContact(newContact);
        return newContact;
    }
    
    public Sale createSale(int vehicleID, String name, String email, String street1, String street2, String city, String state, String zipcode, int price, String type)
    {
        Sale sale = new Sale();
        sale.setVehicleId(vehicleID);
        sale.setName(name);
        sale.setEmail(email);
        sale.setStreet1(street1);
        sale.setStreet2(street2);
        sale.setCity(city);
        sale.setState(state);
        sale.setZipcode(zipcode);
        sale.setPurchasePrice(price);
        sale.setPurchaseType(type);
        
        //sale = dao.addSale();
        return sale;
    }
    
    public List<Special> getAllSpecials()
    {
        return specialDao.getAllSpecials();
    }
    
    public Special addSpecial( Special special)
    {
		/*
		 * Special special = new Special(); special.setTitle(title);
		 * special.setDescription(desc);
		 */
        
        special = specialDao.addSpecial(special);
        return special;
    }
    
    public void deleteSpecial(int id)
    {
        specialDao.deleteSpecialById(id);
        return;
    }

    @Override
    public List<Vehicle> getAllVehiclesForSale() {
        return vehicleDao.getAllVehiclesForSale();
    }
    
    public List<Vehicle> getFeaturedVehicles()
    {
        List<Vehicle> vehicles = vehicleDao.getAllVehicles();
        return vehicles.stream().filter((v) -> v.getFeatured() == true).collect(Collectors.toList());
    }

}
