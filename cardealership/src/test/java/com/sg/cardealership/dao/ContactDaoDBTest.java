package com.sg.cardealership.dao;

import com.sg.cardealership.TestApplicationConfiguration;
import com.sg.cardealership.entity.Contact;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class ContactDaoDBTest {
    
    @Autowired
    ContactDao contactDao;
    
    public ContactDaoDBTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        List<Contact> contacts = contactDao.getAllContacts();
        contacts.forEach(contact -> {
            contactDao.deleteContactById(contact.getContactId());
        });
    }
    
    @AfterEach
    public void tearDown() {
        
    }

    @Test
    public void testAddContactAndGetContactById() {
        Contact contact = new Contact();
        contact.setName("mark Reyes");
        contact.setEmail("mreyes@email.com");
        contact.setPhone("1867655466");
        contact.setMessage("I am only interested in new vehicles");
        contact = contactDao.addContact(contact);

        Contact newContact = contactDao.getContactById(contact.getContactId());

        assertEquals(contact, newContact);
    }
    
    @Test
    public void testGetAllContacts() {
        Contact contact = new Contact();
        contact.setName("mark Reyes");
        contact.setEmail("mreyes@email.com");
        contact.setPhone("1867655466");
        contact.setMessage("I am only interested in new vehicles");
        contact = contactDao.addContact(contact);
        
        Contact contact2 = new Contact();
        contact2.setName("Contact 2");
        contact2.setEmail("contact2@email.com");
        contact2.setPhone("875673634");
        contact2.setMessage("second contact in list");
        contact2 = contactDao.addContact(contact);
        
        List<Contact> contacts = contactDao.getAllContacts();
        
        assertEquals(2, contacts.size());
        assertTrue(contacts.contains(contact));
        assertTrue(contacts.contains(contact2));
    }
    
}
