package com.sg.cardealership.dao;

import com.sg.cardealership.TestApplicationConfiguration;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.User;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class MakeDaoDBTest {
    
    @Autowired
    MakeDao makeDao;
    
    @Autowired
    UserDao userDao;
    
    public MakeDaoDBTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        List<Make> makes = makeDao.getAllMakes();
        makes.forEach(make -> {
            makeDao.deleteMakeById(make.getMakeId());
        });
        
        List<User> users = userDao.getAllUsers();
        users.forEach(user -> {
            userDao.deleteUserById(user.getUserId());
        });
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testAddMakeAndGetMakeById() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Make make = new Make();
        make.setUserId(user.getUserId());
        make.setMake("Audi");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
                            "mm-dd-YYYY");
        LocalDateTime now = LocalDateTime.now();
        make.setDateAdded(now);
        make = makeDao.addMake(make);

        Make newMake = makeDao.getMakeById(make.getMakeId());

        assertEquals(make.getMakeId(), newMake.getMakeId());
    }
    
    @Test
    public void testGetAllMakes() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Make make = new Make();
        make.setUserId(user.getUserId());
        make.setMake("Audi");
        LocalDateTime now = LocalDateTime.now();
        make.setDateAdded(now);
        make = makeDao.addMake(make);
        
        Make make2 = new Make();
        make2.setUserId(user.getUserId());
        make2.setMake("Fiat");
        LocalDateTime now2 = LocalDateTime.now();
        make2.setDateAdded(now2);
        make2 = makeDao.addMake(make2);
        
        List<Make> makes = makeDao.getAllMakes();
        
        assertEquals(2, makes.size());
    }
    
}
