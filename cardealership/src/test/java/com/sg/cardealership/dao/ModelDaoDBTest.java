package com.sg.cardealership.dao;

import com.sg.cardealership.TestApplicationConfiguration;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.Model;
import com.sg.cardealership.entity.User;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class ModelDaoDBTest {
    
    @Autowired
    ModelDao modelDao;
    
    @Autowired
    MakeDao makeDao;
    
    @Autowired
    UserDao userDao;
    
    public ModelDaoDBTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() throws Exception{
        List<Model> models = modelDao.getAllModels();
        models.forEach(model -> {
            modelDao.deleteModelById(model.getModelId());
        });
        
        List<Make> makes = makeDao.getAllMakes();
        makes.forEach(make -> {
            makeDao.deleteMakeById(make.getMakeId());
        });
        
        List<User> users = userDao.getAllUsers();
        users.forEach(user -> {
            userDao.deleteUserById(user.getUserId());
        });
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testAddModelAndGetModelById() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Make make = new Make();
        make.setUserId(user.getUserId());
        make.setMake("Audi");
        LocalDateTime now = LocalDateTime.now();
        make.setDateAdded(now);
        make = makeDao.addMake(make);
        
        Model model = new Model();
        model.setUserId(user.getUserId());
        model.setMakeId(make.getMakeId());
        model.setModel("A4");
        LocalDateTime now2 = LocalDateTime.now();
        model.setDateAdded(now2);
        model = modelDao.addModel(model);

        Model newModel = modelDao.getModelById(model.getModelId());
        
        assertEquals(model.getModelId(), newModel.getModelId());
    }
    
    @Test
    public void testGetAllModels() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Make make = new Make();
        make.setUserId(user.getUserId());
        make.setMake("Audi");
        LocalDateTime now = LocalDateTime.now();
        make.setDateAdded(now);
        make = makeDao.addMake(make);
        
        Model model = new Model();
        model.setUserId(user.getUserId());
        model.setMakeId(make.getMakeId());
        model.setModel("A4");
        LocalDateTime now2 = LocalDateTime.now();
        model.setDateAdded(now2);
        model = modelDao.addModel(model);
        
        Model model2 = new Model();
        model2.setUserId(user.getUserId());
        model2.setMakeId(make.getMakeId());
        model2.setModel("A6");
        LocalDateTime now3 = LocalDateTime.now();
        model2.setDateAdded(now3);
        model2 = modelDao.addModel(model2);
        
        List<Model> models = modelDao.getAllModels();
        
        assertEquals(2, models.size());
    }
    
}
