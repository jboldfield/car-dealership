package com.sg.cardealership.dao;

import com.sg.cardealership.TestApplicationConfiguration;
import com.sg.cardealership.entity.Special;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class SpecialDaoDBTest {
    
    @Autowired
    SpecialDao specialDao;
    
    public SpecialDaoDBTest() {
        
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        List<Special> specials = specialDao.getAllSpecials();
        specials.forEach(special -> {
            specialDao.deleteSpecialById(special.getSpecialId());
        });
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testAddSpecialAndGetSpecialById() {
        Special special = new Special();
        special.setTitle("Special 1");
        special.setDescription("Description for the first special");
        special = specialDao.addSpecial(special);

        Special newSpecial = specialDao.getSpecialById(special.getSpecialId());

        assertEquals(special, newSpecial);
    }
    
    @Test
    public void testGetAllSpecials() {
        Special special = new Special();
        special.setTitle("Special 1");
        special.setDescription("Description for the first special");
        special = specialDao.addSpecial(special);
        
        Special special2 = new Special();
        special2.setTitle("Special 2");
        special2.setDescription("Description for the second special");
        special2 = specialDao.addSpecial(special2);
        
        List<Special> specials = specialDao.getAllSpecials();
        
        assertEquals(2, specials.size());
        assertTrue(specials.contains(special));
        assertTrue(specials.contains(special2));
    }
    
    @Test
    public void testDeleteSpecial() {
        Special special = new Special();
        special.setTitle("Special 1");
        special.setDescription("Description for the first special");
        special = specialDao.addSpecial(special);

        specialDao.deleteSpecialById(special.getSpecialId());

        assertNull(specialDao.getSpecialById(special.getSpecialId()));
    }
    
}
