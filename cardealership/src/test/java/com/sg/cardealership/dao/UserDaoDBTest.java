package com.sg.cardealership.dao;

import com.sg.cardealership.TestApplicationConfiguration;
import com.sg.cardealership.entity.User;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class UserDaoDBTest {
    
    @Autowired
    UserDao userDao;
    
    public UserDaoDBTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        List<User> users = userDao.getAllUsers();
        users.forEach(user -> {
            userDao.deleteUserById(user.getUserId());
        });
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddUserAndGetUserById() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);

        User newUser = userDao.getUserById(user.getUserId());

        assertEquals(user, newUser);
    }
    
    @Test
    public void testGetAllUsers() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        User user2 = new User();
        user2.setFirstName("Austin");
        user2.setLastName("Hill");
        user2.setEmail("ahill@guildcars.com");
        user2.setRole("Sales");
        user2.setPassword("password");
        user2 = userDao.addUser(user2);
        
        List<User> users = userDao.getAllUsers();
        
        assertEquals(2, users.size());
        assertTrue(users.contains(user));
        assertTrue(users.contains(user2));
    }
    
    @Test
    public void testEditUser() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);

        User fromDao = userDao.getUserById(user.getUserId());
        fromDao.setPassword("newpassword");
        userDao.updateUser(fromDao);
        
        assertNotEquals(user, userDao.getUserById(user.getUserId()));
    }
}
