package com.sg.cardealership.dao;

import com.sg.cardealership.TestApplicationConfiguration;
import com.sg.cardealership.entity.Make;
import com.sg.cardealership.entity.Model;
import com.sg.cardealership.entity.User;
import com.sg.cardealership.entity.Vehicle;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author joldf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestApplicationConfiguration.class)
public class VehicleDaoDBTest {
    
    @Autowired
    VehicleDao vehicleDao;
    
    @Autowired
    ModelDao modelDao;
    
    @Autowired
    MakeDao makeDao;
    
    @Autowired
    UserDao userDao;
    
    public VehicleDaoDBTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
        List<Make> makes = makeDao.getAllMakes();
        makes.forEach(make -> {
            makeDao.deleteMakeById(make.getMakeId());
        });
        
        List<Model> models = modelDao.getAllModels();
        models.forEach(model -> {
            modelDao.deleteModelById(model.getModelId());
        });
        
        List<User> users = userDao.getAllUsers();
        users.forEach(user -> {
            userDao.deleteUserById(user.getUserId());
        });
        
        List<Vehicle> vehicles = vehicleDao.getAllVehicles();
        vehicles.forEach(vehicle -> {
            vehicleDao.deleteVehicleById(vehicle.getVehicleId());
        });
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddAndGetVehicleById() {
        User user = new User();
        user.setFirstName("Eric");
        user.setLastName("Wise");
        user.setEmail("ewise@guildcars.com");
        user.setRole("Admin");
        user.setPassword("password");
        user = userDao.addUser(user);
        
        Make make = new Make();
        make.setUserId(user.getUserId());
        make.setMake("Audi");
        LocalDateTime now = LocalDateTime.now();
        make.setDateAdded(now);
        make = makeDao.addMake(make);
        
        Model model = new Model();
        model.setUserId(user.getUserId());
        model.setMakeId(make.getMakeId());
        model.setModel("A4");
        LocalDateTime now2 = LocalDateTime.now();
        model.setDateAdded(now2);
        
        Vehicle vehicle = new Vehicle();
        vehicle.setMakeId(make.getMakeId());
        vehicle.setModelId(model.getModelId());
        vehicle.setType("New");
        vehicle.setBodyStyle("Sedan");
        vehicle.setYear("2021");
        vehicle.setTransmission("Automatic");
        vehicle.setColor("Black");
        vehicle.setInterior("Black");
        vehicle.setMileage(120);
        vehicle.setVin("2DS82019D78S123");
        vehicle.setMsrp(105000);
        vehicle.setSalesPrice(108000);
        vehicle.setDescription("This is a new black Audi A4");
        vehicle.setFeatured(true);
        vehicle = vehicleDao.addVehicle(vehicle);
        
        Vehicle newVehicle = vehicleDao.getVehicleById(vehicle.getVehicleId());

        assertEquals(vehicle, newVehicle);
    }
    
}
